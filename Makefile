MORSE = ./morse
README = README.rst
EXAMPLE_COMMAND = $(MORSE) new sms
EXAMPLE_OUTPUT = newsms.wav

$(README): $(MORSE)
	echo "text to Morse (sound)" > $(README)
	echo "=====================" >> $(README)
	echo >> $(README)
	echo :: >> $(README)
	echo >> $(README)
	echo "  $$ $(MORSE) -h" >> $(README)
	$(MORSE) -h | sed -e 's/^/  /'  >> $(README)
	echo >> $(README)
	echo "example" >> $(README)
	echo "=======" >> $(README)
	echo >> $(README)
	echo "\`\`$(EXAMPLE_COMMAND)\`\` will create the file" >> $(README)
	echo "\`$(EXAMPLE_OUTPUT) <https://gitlab.com/lpirl/text-to-morse-wav/-/jobs/artifacts/master/raw/$(EXAMPLE_OUTPUT)?job=example>\`__" >> $(README)
	echo >> $(README)

example: $(EXAMPLE_OUTPUT)

$(EXAMPLE_OUTPUT): $(MORSE)
	$(EXAMPLE_COMMAND)

clean:
	rm *.wav
