text to Morse (sound)
=====================

::

  $ ./morse -h
  usage: morse [-h] [-p [PADDING]] [-F [FADE_DURATION]] [-d [DOT_DURATION]]
               [-s [SAMPLE_RATE]] [-f [FREQUENCIES [FREQUENCIES ...]]]
               [-o [OUTPUT]]
               WORD [WORD ...]
  
  A quick-and-dirty script to translate text to Morse WAVE files. The default
  sound is intended to be piercing and easy to locate. The WAVE files will be 16
  bits per sample.
  
  positional arguments:
    WORD                  word to translate
  
  optional arguments:
    -h, --help            show this help message and exit
    -p [PADDING], --padding [PADDING]
                          silence at beginning and end of file in seconds
                          (default: 0.1)
    -F [FADE_DURATION], --fade-duration [FADE_DURATION]
                          fade duration as fraction of dot duration (default:
                          0.1)
    -d [DOT_DURATION], --dot-duration [DOT_DURATION]
                          duration of a "dot" in seconds (default: 0.1)
    -s [SAMPLE_RATE], --sample-rate [SAMPLE_RATE]
                          output sample rate in Hz (default: 48000)
    -f [FREQUENCIES [FREQUENCIES ...]], --frequencies [FREQUENCIES [FREQUENCIES ...]]
                          frequencies to use for tone generation (default: [809,
                          3701, 7501])
    -o [OUTPUT], --output [OUTPUT]
                          file to write to (default: None)

example
=======

``./morse new sms`` will create the file
`newsms.wav <https://gitlab.com/lpirl/text-to-morse-wav/-/jobs/artifacts/master/raw/newsms.wav?job=example>`__

